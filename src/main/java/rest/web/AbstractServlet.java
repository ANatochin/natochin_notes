package rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

public class AbstractServlet extends HttpServlet {

    protected ObjectMapper objectMapper = new ObjectMapper();

    public void servletResponse(HttpServletResponse response, int status, String message) throws IOException {

        response.setStatus(status);
        response.setContentType("application/json");
        response.getWriter().write(message);
        response.getWriter().flush();

    }
}
