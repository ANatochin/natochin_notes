package rest.web;

import exceptions.ApplicationException;
import rest.dto.NoteDeleteDto;
import rest.dto.NoteDto;
import rest.dto.ResponseDto;
import rest.entity.UserEntity;
import rest.validator.NoteValidator;
import services.LoginService;
import services.NoteService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/note/delete")
public class NoteDeleteServlet extends AbstractServlet {

    private LoginService loginService = new LoginService();
    private NoteService noteService = new NoteService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        NoteDeleteDto noteDeleteDto = this.objectMapper.readValue(request.getInputStream(), NoteDeleteDto.class);

        try{

            UserEntity user = this.noteService.getUserByLogin(noteDeleteDto.getLogin());
            this.noteService.deleteNote(user.getLogin(), noteDeleteDto.getName());

            servletResponse(response, 200, user.toString()+" deleted note");

        }catch(ApplicationException e){

            servletResponse(response, 400, this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
