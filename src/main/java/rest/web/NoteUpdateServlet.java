package rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.ApplicationException;
import rest.dto.NoteDto;
import rest.dto.ResponseDto;
import rest.entity.UserEntity;
import rest.validator.NoteValidator;
import services.LoginService;
import services.NoteService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/note/update")
public class NoteUpdateServlet extends AbstractServlet {

    private LoginService loginService = new LoginService();
    private NoteService noteService = new NoteService();
    private NoteValidator noteValidator = new NoteValidator();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        NoteDto noteDto = this.objectMapper.readValue(request.getInputStream(), NoteDto.class);

        try {
            this.noteValidator.validate(noteDto);
        } catch (ApplicationException e) {
            throw new RuntimeException(e);
        }

        try{

            UserEntity user = this.noteService.getUserByLogin(noteDto.getLogin());
            this.noteService.updateNote(user.getLogin(), noteDto.getName(),noteDto.getText());

            servletResponse(response, 200, user.toString()+" updated note");

        }catch(ApplicationException e){

            servletResponse(response, 400, this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
