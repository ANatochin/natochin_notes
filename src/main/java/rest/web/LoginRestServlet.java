package rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.ApplicationException;
import rest.dto.LoginDto;
import rest.dto.ResponseDto;
import rest.entity.UserEntity;
import services.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/login", loadOnStartup = 1)
public class LoginRestServlet extends AbstractServlet {

    private ObjectMapper objectMapper = new ObjectMapper();
    private LoginService loginService = new LoginService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // reading JSON request by Mapper and providing it to DTO object for further operations
        LoginDto loginDto = this.objectMapper.readValue(request.getInputStream(), LoginDto.class);

        try{

            UserEntity user = this.loginService.login(loginDto.getLogin(), loginDto.getPassword());

            servletResponse(response, 200, user.toString()+" ENTERED!");

        }catch(ApplicationException e){

            servletResponse(response, 400, this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
