package rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.ApplicationException;
import rest.dto.RegistrationDto;
import rest.dto.ResponseDto;
import rest.entity.UserEntity;
import rest.validator.RegistrationValidator;
import services.LoginService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/register")
public class RegisterRestServlet extends AbstractServlet {
    private RegistrationValidator registrationValidator = new RegistrationValidator();
    private LoginService loginService = new LoginService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RegistrationDto registrationDto = this.objectMapper.readValue(request.getInputStream(), RegistrationDto.class);

        try{

            UserEntity user = new UserEntity(registrationDto.getLogin(),
                    registrationDto.getLastName(),
                    registrationDto.getFirstName(),
                    registrationDto.getPassword());

            this.registrationValidator.validate(registrationDto);

            this.loginService.register(registrationDto.getLogin(),
                    registrationDto.getLastName(),
                    registrationDto.getFirstName(),
                    registrationDto.getPassword());

            servletResponse(response, 200, user.toString()+" CREATED!");

        }catch(ApplicationException e){

            servletResponse(response, 400, this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
