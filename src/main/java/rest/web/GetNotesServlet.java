package rest.web;

import exceptions.ApplicationException;
import rest.dto.ResponseDto;
import rest.dto.TableDto;
import services.NoteService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/note/get/notes")
public class GetNotesServlet extends AbstractServlet {
    NoteService noteService = new NoteService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TableDto tableDto = this.objectMapper.readValue(request.getInputStream(),TableDto.class);

        try {
            this.noteService.getAllNotes();

            servletResponse(response, 200, "all notes");

        }catch(ApplicationException e){

            servletResponse(response, 400, this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
