package rest.entity;

public class UserEntity {
    private String login;
    private String lastName;
    private String firstName;
    private String password;
    private int id;

    public UserEntity(){

    }

    public UserEntity(String login, String lastName, String firstName, String password) {
        this.login = login;
        this.lastName = lastName;
        this.firstName = firstName;
        this.password = password;
    }
    public UserEntity(int id, String login, String lastName, String firstName, String password) {
        this(login, lastName,firstName,password);
        this.id = id;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString(){
        return "user:" + this.firstName + " " + this.lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
