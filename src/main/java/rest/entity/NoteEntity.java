package rest.entity;

public class NoteEntity {

    private String login;
    private String noteName;
    private String text;

    public NoteEntity(String login, String noteName, String text) {
        this.login = login;
        this.noteName = noteName;
        this.text = text;
    }

    public String getNoteName() {
        return noteName;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString(){
        return this.getNoteName() + " : " + this.getText();
    }

}
