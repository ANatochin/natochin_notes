package rest.dto;

public class ResponseDto {
    private String message;

    public ResponseDto(String msg){
        this.message = msg;
    }

    public String getMessage() {
        return message;
    }
}
