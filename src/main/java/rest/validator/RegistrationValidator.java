package rest.validator;

import exceptions.ApplicationException;
import rest.dto.RegistrationDto;

public class RegistrationValidator implements Validator<RegistrationDto> {


    @Override
    public void validate(RegistrationDto dto) throws ApplicationException {

        if (dto.getLogin() == null || dto.getLogin().isEmpty()) {
            throw new ApplicationException("Enter login");
        }

        if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
            throw new ApplicationException("Enter password");
        }

        if (dto.getPasswordConfirm() == null || dto.getPasswordConfirm().isEmpty()) {
            throw new ApplicationException("Repeat password");
        }

        if (!dto.getPassword().equals(dto.getPasswordConfirm())) {
            throw new ApplicationException("Entered passwords are not similar");
        }


    }
}
