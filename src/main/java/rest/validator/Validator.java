package rest.validator;

import exceptions.ApplicationException;

public interface Validator<T> {
    void validate(T dto) throws ApplicationException;
}
