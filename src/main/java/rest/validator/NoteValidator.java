package rest.validator;

import exceptions.ApplicationException;
import rest.dto.NoteDto;

public class NoteValidator implements Validator<NoteDto> {
    @Override
    public void validate(NoteDto dto) throws ApplicationException {

        if (dto.getLogin() == null || dto.getLogin().isEmpty()) {
            throw new ApplicationException("Enter login");
        }

        if(dto.getName() == null || dto.getName().isEmpty()){
            throw new ApplicationException("Note name could not be empty");
        }

        if(dto.getText() == null || dto.getText().isEmpty()){
            throw new ApplicationException("Pls enter any text");
        }

    }
}
