package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import exceptions.ApplicationException;
import rest.entity.UserEntity;

public class LoginService {



    public UserEntity login(String login, String password) throws ApplicationException, SQLException {
        UserEntity user = null;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NotesTaskDB", "postgres", "root")){

            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM \"users\" WHERE login = ? AND password = ?")){

                statement.setString(1, login);
                statement.setString(2, password);

                try(ResultSet resultSet = statement.executeQuery()){
                    while (resultSet.next()) {
                        user = new UserEntity(
                                resultSet.getString("login"),
                                resultSet.getString("last_name"),
                                resultSet.getString("first_name"),
                                resultSet.getString("password")
                        );
                    }
                }
            }
        }

        return user;
    }

    public void register(String login, String lastName, String firstName, String password) throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NotesTaskDB", "postgres", "root")){

            try(PreparedStatement prepStatement = connection.prepareStatement(
                    "INSERT INTO \"users\" (login, last_name,first_name,password) VALUES (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS)){

                prepStatement.setString(1, login);
                prepStatement.setString(2, lastName);
                prepStatement.setString(3, firstName);
                prepStatement.setString(4, password);

                prepStatement.executeUpdate();

            }catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
