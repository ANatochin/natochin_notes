package services;

import exceptions.ApplicationException;
import rest.entity.NoteEntity;
import rest.entity.UserEntity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class NoteService {

    public UserEntity getUserByLogin(String login) throws SQLException, ApplicationException {

        UserEntity user = null;

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NotesTaskDB", "postgres", "root")){

            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM \"users\" WHERE login = ?")){

                statement.setString(1, login);

                try(ResultSet resultSet = statement.executeQuery()){
                    while (resultSet.next()) {
                        user = new UserEntity(
                                resultSet.getString("login"),
                                resultSet.getString("last_name"),
                                resultSet.getString("first_name"),
                                resultSet.getString("password")
                        );
                    }
                }
            }
        }
        return user;
    }

    public void createNote(String name, String text, String login) throws SQLException {

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NotesTaskDB", "postgres", "root")){

            try(PreparedStatement prepStatement = connection.prepareStatement(
                    "INSERT INTO \"notes\" (note_name,note_text, user_login) VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS)){

                prepStatement.setString(1, name);
                prepStatement.setString(2, text);
                prepStatement.setString(3, login);

                prepStatement.executeUpdate();

            }catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void updateNote(String login, String name, String text) throws SQLException {

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NotesTaskDB", "postgres", "root")){

            try(PreparedStatement prepStatement = connection.prepareStatement(
                    "UPDATE \"notes\" SET note_text = ? WHERE user_login = ? AND note_name = ?",
                    Statement.RETURN_GENERATED_KEYS)){

                prepStatement.setString(1, text);
                prepStatement.setString(2, login);
                prepStatement.setString(3, name);

                prepStatement.executeUpdate();

            }catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteNote(String login, String name) throws SQLException {

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NotesTaskDB", "postgres", "root")){

            try(PreparedStatement prepStatement = connection.prepareStatement(
                    "DELETE FROM \"notes\" WHERE user_login = ? AND note_name = ?",
                    Statement.RETURN_GENERATED_KEYS)){

                prepStatement.setString(1, login);
                prepStatement.setString(2, name);

                prepStatement.executeUpdate();

            }catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void getAllNotes() throws SQLException, ApplicationException {

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NotesTaskDB", "postgres", "root")){

            try (Statement statement = connection.createStatement()) {

                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM \"notes\"")) {

                    while (resultSet.next()) {

                        NoteEntity note = new NoteEntity(
                                resultSet.getString("user_login"),
                                resultSet.getString("note_name"),
                                resultSet.getString("note_text")
                        );

                        System.out.printf(
                                "%s: %s\n",
                                resultSet.getRow(),
                                note
                        );
                    }
                }

            }catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
